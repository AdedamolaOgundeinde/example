<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('people_name');
            $table->string('people_phone', 15)->unique();
            $table->smallInteger('people_age')->nullable();
            $table->enum('people_gender', ['Male', 'Female'])->nullable();
            $table->string('people_email')->nullable();
            $table->boolean('people_status')->default(true);
            $table->integer('people_lga_id')->unsigned();
            $table->integer('people_poll_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
