<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ajani Opeyemi',
            'email' => 'opesco_1992@yahoo.com',
            'password' => bcrypt('12345678'),
            'is_admin' => true //this is a user admin
        ]);
    }
}
