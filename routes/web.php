<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home'); //the dashboard

Route::group([
    'prefix'=>'/admin/agents',
    'middleware'=>['auth', 'admin.only']
], function(){
    Route::get('/edit/{id?}', 'AgentsController@edit')->name('admin.agents.edit');
    Route::post('/process_/{id?}', 'AgentsController@add_edit')->name('admin.agents.process_');
    Route::get('/manage', 'AgentsController@manage')->name('admin.agents.manage');
});

Route::group([
    'prefix'=>'/admin/states',
    'middleware'=>['auth', 'admin.only']
], function(){
    Route::get('/edit/{id?}', 'StatesController@edit')->name('admin.states.edit');
    Route::post('/process_/{id?}', 'StatesController@add_edit')->name('admin.states.process_');
    Route::get('/manage', 'StatesController@manage')->name('admin.states.manage');
});

Route::group([
    'prefix'=>'/admin/lgas',
    'middleware'=>['auth', 'admin.only']
], function(){
    Route::get('/edit/{id?}', 'LgasController@edit')->name('admin.lgas.edit');
    Route::post('/process_/{id?}', 'LgasController@add_edit')->name('admin.lgas.process_');
    Route::get('/manage', 'LgasController@manage')->name('admin.lgas.manage');
});

Route::group([
    'prefix'=>'/admin/wards',
    'middleware'=>['auth', 'admin.only']
], function(){
    Route::get('/edit/{id?}', 'WardsController@edit')->name('admin.wards.edit');
    Route::post('/process_/{id?}', 'WardsController@add_edit')->name('admin.wards.process_');
    Route::get('/manage', 'WardsController@manage')->name('admin.wards.manage');
});

Route::group([
    'prefix'=>'/admin/polls',
    'middleware'=>['auth', 'admin.only']
], function(){
    Route::get('/edit/{id?}', 'PollsController@edit')->name('admin.polls.edit');
    Route::post('/process_/{id?}', 'PollsController@add_edit')->name('admin.polls.process_');
    Route::get('/manage', 'PollsController@manage')->name('admin.polls.manage');
});

Route::group([
    'prefix'=>'/admin/people',
    'middleware'=>['auth', 'admin.only']
], function(){
    Route::get('/edit/{id?}', 'PeoplesController@edit')->name('admin.peoples.edit');
    Route::post('/process_/{id?}', 'PeoplesController@add_edit')->name('admin.peoples.process_');
    Route::get('/manage', 'PeoplesController@manage')->name('admin.peoples.manage');
    Route::get('/delete/{id}', 'PeoplesController@manage')->name('admin.peoples.delete');
});

Route::group([
    'prefix'=>'/admin/messaging',
    'middleware'=>['auth', 'admin.only']
], function(){
    Route::get('/new', 'MessagingController@create')->name('admin.messaging.create');
    Route::post('/new/process', 'MessagingController@create')->name('admin.messaging.create.process_');
    Route::get('/preview/{id}', 'MessagingController@preview')->name('admin.messaging.preview');
    Route::get('/manage', 'MessagingController@manage')->name('admin.messaging.manage');
});
