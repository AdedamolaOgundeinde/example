<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Lga;
use App\State;

class LgasController extends Controller
{
  public function edit(Request $request, $id=null){
    $lga = null;
    $states = [''=>'Please select a State'];
    if($id) {
      $lga = Lga::find($id);
      if($lga===null){
        return redirect(route('admin.lgas.manage'));
      }
    }
    collect(State::where('state_status', true)->get())->each(function($one) use (&$states){
        $states[$one->id] = $one->state_name;
    });
      return view('lgas.edit',[
        'id'=>$id,
        'lga'=>$lga,
        'states'=> $states
      ]);
  }

  public function add_edit(Request $request, $id=null){
    $validate = Validator::make($request->all(), [
      'lga_name'=> 'required|min:2',
      'lga_state_id'=> 'required|min:1'
    ]);
    if($validate->fails()){
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Request failed validation'
      ]);
    }
    if($id){
      //it's an update
      $lga = Lga::find($id);
      if($lga){
        $lga->lga_name=ucfirst($request->input('lga_name'));
        $lga->lga_state_id=$request->input('lga_state_id');
        $lga->lga_status= $request->input('lga_status') || false;
        $lga->save();
        return back()->withErrors([
          'status'=>'success',
          'message'=>'Record updated successfully'
        ]);
      }
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Record not found'
      ]);
    }
    else{
      //it's a new agent
      Lga::create([
        'lga_name' => ucfirst($request->input('lga_name')),
        'lga_state_id' => $request->input('lga_state_id'),
        'lga_status' => $request->input('lga_status') || false
      ]);
      return back()->withErrors([
        'status'=>'success',
        'message'=>'Record added successfully'
      ]);

    }
  }

    public function manage(\App\DataTables\LgasDataTable $dataTable){
        if (request()->ajax()) {
            return $dataTable->ajax(); //Datatables::of(Staff::query())->make(true);
        }
        //$dataTable->tester();
        return $dataTable->render('lgas.manage');
    }
}
