<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\State;

class StatesController extends Controller
{
  public function edit(Request $request, $id=null){
    $state = null;
    if($id) {
      $state = State::find($id);
      if($state===null){
        return redirect(route('admin.states.manage'));
      }
    }
      return view('states.edit',[
        'id'=>$id,
        'state'=>$state
      ]);
  }

  public function add_edit(Request $request, $id=null){
    $validate = Validator::make($request->all(), [
      'state_name'=> 'required|min:2'
    ]);
    if($validate->fails()){
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Request failed validation'
      ]);
    }
    if($id){
      //it's an update
      $state = State::find($id);
      if($state){
        $state->state_name=ucfirst($request->input('state_name'));
        $state->state_status= $request->input('state_status') || false;
        $state->save();
        return back()->withErrors([
          'status'=>'success',
          'message'=>'Record updated successfully'
        ]);
      }
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Record not found'
      ]);
    }
    else{
      //it's a new agent
      State::create([
        'state_name' => ucfirst($request->input('state_name')),
        'state_status' => $request->input('state_status') || false
      ]);
      return back()->withErrors([
        'status'=>'success',
        'message'=>'Record added successfully'
      ]);

    }
  }

    public function manage(\App\DataTables\StatesDataTable $dataTable){
        if (request()->ajax()) {
            return $dataTable->ajax(); //Datatables::of(Staff::query())->make(true);
        }
        //$dataTable->tester();
        return $dataTable->render('states.manage');
    }
}
