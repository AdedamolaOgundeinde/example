<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Ward;
use App\State;
use App\Lga;

class WardsController extends Controller
{
  public function edit(Request $request, $id=null){
    $ward = null;
    $states = [''=>'Please select a State'];
    $lgas = [''=>'Please select a State first'];
    if($id) {
      $ward = Ward::find($id);
      if($ward===null){
        return redirect(route('admin.wards.manage'));
      }
    }
    collect(State::where('state_status', true)->get())->each(function($one) use (&$states){
        $states[$one->id] = $one->state_name;
    });
    if($request->query('state')){
        $lgas = [''=>'Please select a LGA'];
        collect(Lga::where('lga_status', true)->where('lga_state_id', $request->query('state'))->get())
                ->each(function($one) use (&$lgas){
            $lgas[$one->id] = $one->lga_name;
        });
    }
      return view('wards.edit',[
        'id'=>$id,
        'ward'=>$ward,
        'states'=>$states,
        'lgas'=>$lgas
      ]);
  }

  public function add_edit(Request $request, $id=null){
    $validate = Validator::make($request->all(), [
      'ward_name'=> 'required|min:2',
      'ward_lga_id'=> 'required|min:1'
    ]);
    if($validate->fails()){
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Request failed validation'
      ]);
    }
    if($id){
      //it's an update
      $ward = Lga::find($id);
      if($ward){
        $ward->ward_name=ucfirst($request->input('ward_name'));
        $ward->ward_status= $request->input('ward_status') || false;
        $ward->ward_lga_id= $request->input('ward_lga_id');
        $ward->save();
        return back()->withErrors([
          'status'=>'success',
          'message'=>'Record updated successfully'
        ]);
      }
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Record not found'
      ]);
    }
    else{
      //it's a new agent
      Ward::create([
        'ward_name' => ucfirst($request->input('ward_name')),
        'ward_status' => $request->input('ward_status') || false,
        'ward_lga_id' => $request->input('ward_lga_id'),
      ]);
      return back()->withErrors([
        'status'=>'success',
        'message'=>'Record added successfully'
      ]);

    }
  }

    public function manage(\App\DataTables\WardsDataTable $dataTable){
        if (request()->ajax()) {
            return $dataTable->ajax(); //Datatables::of(Staff::query())->make(true);
        }
        //$dataTable->tester();
        return $dataTable->render('wards.manage');
    }
}
