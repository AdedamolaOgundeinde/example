<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\People;
use App\State;
use App\Lga;

class PeoplesController extends Controller
{
  public function edit(Request $request, $id=null){
    $people = null;
    $states = [''=>'Please select a State'];
    $lgas = [''=>'Please select a State first'];
    if($id) {
      $people = People::find($id);
      if($people===null){
        return redirect(route('admin.peoples.manage'));
      }
    }
    collect(State::where('state_status', true)->get())->each(function($one) use (&$states){
        $states[$one->id] = $one->state_name;
    });
    if($request->query('state')){
        $lgas = [''=>'Please select a LGA'];
        collect(Lga::where('lga_status', true)->where('lga_state_id', $request->query('state'))->get())
                ->each(function($one) use (&$lgas){
            $lgas[$one->id] = $one->lga_name;
        });
    }
      return view('peoples.edit',[
        'id'=>$id,
        'people'=>$people,
        'states'=>$states,
        'lgas'=>$lgas
      ]);
  }

  public function add_edit(Request $request, $id=null){
    //dd($request->all());
    $validate = Validator::make($request->all(), [
      'people_name'=> 'required|min:2',
      'people_phone'=> 'required|unique:people,people_phone',
      'people_age'=> 'numeric|between:18,200',
      'people_gender'=> 'required|in:Male,Female',
      'people_lga_id'=> 'required'
    ]);
    if($validate->fails()){
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Request failed validation :'.$validate->errors()->first()
      ])->withInput();
    }
    if($id){
      //it's an update
      $people = People::find($id);
      if($people){
        $people->people_name=ucfirst($request->input('people_name'));
        $people->people_phone=$request->input('people_phone');
        $people->people_age=$request->input('people_age');
        $people->people_gender=$request->input('people_gender');
        $people->people_email=$request->input('people_email');
        $people->people_lga_id=$request->input('people_lga_id');
        $people->people_status= $request->input('people_status') || false;
        $people->save();
        return back()->withErrors([
          'status'=>'success',
          'message'=>'Record updated successfully'
        ]);
      }
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Record not found'
      ]);
    }
    else{
      //it's a new agent
      People::create([
        'people_name' => ucfirst($request->input('people_name')),
        'people_phone' => $request->input('people_phone'),
        'people_age' => $request->input('people_age'),
        'people_gender' => $request->input('people_gender'),
        'people_email' => $request->input('people_email'),
        'people_lga_id' => $request->input('people_lga_id'),
        'people_status' => $request->input('people_status') || false
      ]);
      return back()->withErrors([
        'status'=>'success',
        'message'=>'Record added successfully'
      ]);

    }
  }

    public function manage(\App\DataTables\PeoplesDataTable $dataTable){
        if (request()->ajax()) {
            return $dataTable->ajax(); //Datatables::of(Staff::query())->make(true);
        }
        
        $states = ['0'=>'All'];
        $lgas = ['0'=>'All'];
        collect(State::where('state_status', true)->get())->each(function($one) use (&$states){
            $states[$one->id] = $one->state_name;
        });
        if(request()->query('state')){
            collect(Lga::where('lga_status', true)->where('lga_state_id', request()->query('state'))->get())
                    ->each(function($one) use (&$lgas){
                $lgas[$one->id] = $one->lga_name;
            });
        }
        return $dataTable->render('peoples.manage', [
            'states'=> $states,
            'lgas'=> $lgas
        ]);
    }
}
