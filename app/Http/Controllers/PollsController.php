<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Poll;
use App\State;
use App\Lga;
use App\Ward;

class PollsController extends Controller
{
  public function edit(Request $request, $id=null){
    $poll = null;
    $states = [''=>'Please select a State'];
    $lgas = [''=>'Please select a State first'];
    $wards = [''=>'Please select a LGA first'];
    if($id) {
      $poll = Poll::find($id);
      if($poll===null){
        return redirect(route('admin.polls.manage'));
      }
    }
    collect(State::where('state_status', true)->get())->each(function($one) use (&$states){
        $states[$one->id] = $one->state_name;
    });
    if($request->query('state')){
        $lgas = [''=>'Please select a LGA'];
        collect(Lga::where('lga_status', true)->where('lga_state_id', $request->query('state'))->get())
                ->each(function($one) use (&$lgas){
            $lgas[$one->id] = $one->lga_name;
        });
    }
    if($request->query('lga')){
        $wards = [''=>'Please select a Ward'];
        collect(Ward::where('ward_status', true)->where('ward_lga_id', $request->query('lga'))->get())
                ->each(function($one) use (&$wards){
            $wards[$one->id] = $one->ward_name;
        });
    }
      return view('polls.edit',[
        'id'=>$id,
        'poll'=>$poll,
        'states'=> $states,
        'lgas' => $lgas,
        'wards' => $wards
      ]);
  }

  public function add_edit(Request $request, $id=null){
    $validate = Validator::make($request->all(), [
      'poll_name'=> 'required|min:2',
      'poll_ward_id'=> 'required'
    ]);
    if($validate->fails()){
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Request failed validation'
      ]);
    }
    if($id){
      //it's an update
      $poll = Poll::find($id);
      if($poll){
        $poll->poll_name=ucfirst($request->input('poll_name'));
        $poll->poll_ward_id=$request->input('poll_ward_id');
        $poll->poll_status= $request->input('poll_status') || false;
        $poll->save();
        return back()->withErrors([
          'status'=>'success',
          'message'=>'Record updated successfully'
        ]);
      }
      return back()->withErrors([
        'status'=>'danger',
        'message'=>'Record not found'
      ]);
    }
    else{
      //it's a new agent
      Poll::create([
        'poll_name' => ucfirst($request->input('poll_name')),
        'poll_ward_id' => $request->input('poll_ward_id'),
        'poll_status' => $request->input('poll_status') || false
      ]);
      return back()->withErrors([
        'status'=>'success',
        'message'=>'Record added successfully'
      ]);

    }
  }

    public function manage(\App\DataTables\PollsDataTable $dataTable){
        if (request()->ajax()) {
            return $dataTable->ajax(); //Datatables::of(Staff::query())->make(true);
        }
        //$dataTable->tester();
        return $dataTable->render('polls.manage');
    }
}
