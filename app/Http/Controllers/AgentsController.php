<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AgentsController extends Controller
{

    public function edit(Request $request, $id=null){
      $agent = null;
      if($id) {
        $agent = Agent::with(['user', 'poll'])->find($id);
        if($agent===null){
          return redirect(route('admin.agents.manage'));
        }
      }
        return view('agents.edit',[
          'id'=>$id,
          'agent'=>$agent
        ]);
    }

    public function add_edit(Request $request, $id=null){
      $validate = Validator::make($request->all(), [
        'agent_poll_id'=> 'required'
      ]);
      if($validate->fails()){
        return back()->withErrors([
          'status'=>'danger',
          'message'=>'Request failed validation'
        ]);
      }
      if($id){
        //it's an update
        $agent = Agent::find($id);
        if($agent){
          $agent->save();
          return back()->withErrors([
            'status'=>'success',
            'message'=>'Record updated successfully'
          ]);
        }
        return back()->withErrors([
          'status'=>'danger',
          'message'=>'Record not found'
        ]);
      }
      else{
        //it's a new agent
          //TODO, not completed yet
//        Agent::create([
//          'agent_poll_id'=>$request->input('agent_poll_id')
//        ]);
        return back()->withErrors([
          'status'=>'success',
          'message'=>'Record added successfully'
        ]);
      }
    }
    
    public function manage(\App\DataTables\AgentsDataTable $dataTable){

        if (request()->ajax()) {
            return $dataTable->ajax(); //Datatables::of(Staff::query())->make(true);
        }
        //$dataTable->tester();
        return $dataTable->render('agents.manage');

    }
}
