<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lga extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lga_name', 'lga_status', 'lga_state_id'
    ];
    
    public function state(){
        return $this->hasOne('App\State', 'id', 'lga_state_id');
    }
    
    public function wards(){
        return $this->hasMany('App\Ward', 'ward_lga_id');
    }
    
    public function people(){
        return $this->hasMany('App\Person', 'person_lga_id');
    }
}
