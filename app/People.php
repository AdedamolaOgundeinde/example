<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    
    protected $table = 'people';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'people_name', 'people_phone', 'people_age', 'people_gender',
        'people_age', 'people_email', 'people_status', 'people_lga_id',
        'people_poll_id'
    ];
    
    public function lga(){
        return $this->hasOne('App\Lga', 'id', 'people_lga_id');
    }
    
    public function poll(){
        return $this->hasOne('App\Poll', 'id', 'people_poll_id');
    }
}
