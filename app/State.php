<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state_name', 'state_status'
    ];
    
    public function lgas(){
        return $this->hasMany('App\Lga', 'lga_state_id', 'state_id');
    }
}
