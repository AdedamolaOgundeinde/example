<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ward_name', 'ward_status', 'ward_lga_id'
    ];
    
    public function lga(){
        return $this->hasOne('App\Lga', 'id', 'ward_lga_id');
    }
    
    public function polls(){
        return $this->hasMany('App\Poll', 'poll_ward_id');
    }
}
