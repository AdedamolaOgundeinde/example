<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'poll_name', 'poll_status', 'poll_ward_id'
    ];
    
    public function ward(){
        return $this->hasOne('App\Ward', 'id', 'poll_ward_id');
    }

    public function agents(){
        return $this->hasMany('App\Agent', 'agent_poll_id');
    }
    
    public function person(){
        return $this->hasMany('App\Person', 'person_poll_id');
    }
}
