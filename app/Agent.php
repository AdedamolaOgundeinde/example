<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agent_poll_id'
    ];
    
    public function user(){
        return $this->hasOne('App\User', 'user_agent_id', 'agent_id');
    }
    
    public function poll(){
        return $this->hasOne('App\Poll', 'id', 'agent_poll_id');
    }
}
