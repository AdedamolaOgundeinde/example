<?php

namespace App\DataTables;

use App\Poll;
use Yajra\Datatables\Services\DataTable;

class PollsDataTable extends DataTable
{

    private $route_definition = 'admin.polls.edit';
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
            //->addColumn('action', 'pollsdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
       $query = Poll::query()->select('polls.id as id', 'poll_name', 'poll_status', 
                'polls.created_at as created_at', 'polls.updated_at as updated_at',
                'lgas.lga_name as lga', 'states.state_name as state', 
               'wards.ward_name as ward','states.id as state_id', 'lgas.id as lga_id')
               ->leftJoin('wards', 'polls.poll_ward_id', 'wards.id')
                ->leftJoin('lgas', 'wards.ward_lga_id', 'lgas.id')
                ->leftJoin('states', 'lgas.lga_state_id', 'states.id');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->removeColumn('id')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
                        'buttons' => [
                            'reload',
                        ],
                    ]);
    }

    public function ajax(){
        return $this->datatables->eloquent($this->query())
            ->addColumn('action', function($one) {
                $menu = '<a href="'.route($this->route_definition, 
                        ['id'=>$one->id, 'state'=>$one->state_id, 'lga'=>$one->lga_id]).'" title="Edit"><i class="fa fa-edit"></i></a>';
                return $menu;
            })->make(true);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'poll_name',
            'ward'=>['searchable'=>false],
            'lga'=>['searchable'=>false],
            'state'=>['searchable'=>false],
            'poll_status',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'pollsdatatable_' . time();
    }
}
