<?php

namespace App\DataTables;

use App\Lga;
use Yajra\Datatables\Services\DataTable;

class LgasDataTable extends DataTable
{

    private $route_definition = 'admin.lgas.edit';
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
            //->addColumn('action', 'lgasdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Lga::query()->select('lgas.id as id', 'lga_name', 'lga_status', 
                'lgas.created_at as created_at', 'lgas.updated_at as updated_at',
                'states.state_name as state')
                ->leftJoin('states', 'lgas.lga_state_id', 'states.id');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
                        'buttons' => [
                            'reload',
                        ],
                    ]);
    }

    public function ajax(){
        return $this->datatables->eloquent($this->query())
            ->addColumn('action', function($one) {
                $menu = '<a href="'.route($this->route_definition, $one->id).'" title="Edit"><i class="fa fa-edit"></i></a>';
                return $menu;
            })->make(true);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'lga_name',
            'state'=>['searchable'=>false],
            'lga_status',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'lgasdatatable_' . time();
    }
}
