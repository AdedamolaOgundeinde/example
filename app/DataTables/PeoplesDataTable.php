<?php

namespace App\DataTables;

use App\People;
use Yajra\Datatables\Services\DataTable;

class PeoplesDataTable extends DataTable
{

    private $route_definition = 'admin.peoples.edit';
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
            //->addColumn('action', 'peoplesdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
       $query = People::query()->select('people.id as id', 'people_name as name', 'people_status as status', 
               'people_phone as phone', 'people_email as email', 'people_gender as gender', 'people_age as age',
                'people.created_at as created_at', 'people.updated_at as updated_at',
                'lgas.lga_name as lga', 'states.state_name as state', 'states.id as state_id',
               'lgas.id as lga_id')
                ->leftJoin('lgas', 'people.people_lga_id', 'lgas.id')
                ->leftJoin('states', 'lgas.lga_state_id', 'states.id');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->removeColumn('id')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
                        'buttons' => [
                            'reload',
                        ],
                    ]);
    }

    public function ajax(){
        return $this->datatables->eloquent($this->query())
            ->addColumn('action', function($one) {
                $menu = '<a href="'.route($this->route_definition, 
                        ['id'=>$one->id, 'state'=>$one->state_id]).'" title="Edit"><i class="fa fa-edit"></i></a>'.
                        '<a href="'.route('admin.peoples.delete', ['id'=>$one->id]).'" title="Delete"><i class="fa fa-trash"></i></a>';
                return $menu;
            })->make(true);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'phone',
            'email',
            'age',
            'gender',
            'lga'=>['searchable'=>false],
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'peoplesdatatable_' . time();
    }
}
