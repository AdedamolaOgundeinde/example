<?php

namespace App\DataTables;

use App\Ward;
use Yajra\Datatables\Services\DataTable;

class WardsDataTable extends DataTable
{

    private $route_definition = 'admin.wards.edit';
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
            //->addColumn('action', 'wardsdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Ward::query()->select('wards.id as id', 'ward_name', 'ward_status', 
                'wards.created_at as created_at', 'wards.updated_at as updated_at',
                'lgas.lga_name as lga', 'states.state_name as state', 'states.id as state_id')
                ->leftJoin('lgas', 'wards.ward_lga_id', 'lgas.id')
                ->leftJoin('states', 'lgas.lga_state_id', 'states.id');

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
                        'buttons' => [
                            'reload',
                        ],
                    ]);
    }

    public function ajax(){
        return $this->datatables->eloquent($this->query())
            ->addColumn('action', function($one) {
                $menu = '<a href="'.route($this->route_definition, 
                        ['id'=>$one->id, 'state'=>$one->state_id]).'" title="Edit"><i class="fa fa-edit"></i></a>';
                return $menu;
            })->make(true);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'ward_name',
            'lga'=>['searchable'=>false],
            'state'=>['searchable'=>false],
            'ward_status',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'wardsdatatable_' . time();
    }
}
