<?php

namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;

class AgentsDataTable extends DataTable
{
    
    private $route_definition = 'admin.agents.edit';
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query());
            //->addColumn('action', 'agentsdatatable.action');
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
       $query = User::query()->select('agents.id as id', 'name', 'email', 'agents.created_at as created_at', 'agents.updated_at as updated_at')
                ->leftJoin('agents', 'users.user_agent_id', 'agents.id')
               ->where('is_admin', false);
        
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax('')
                    ->removeColumn('id')
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'order'   => [[0, 'desc']],
                        'buttons' => [
                            'reload',
                        ],
                    ]);
    }
    
    public function ajax(){
        return $this->datatables->eloquent($this->query())
            ->addColumn('action', function($one) { 
                $menu = '<a href="'.route($this->route_definition, $one->id).'" title="Edit"><i class="fa fa-edit"></i></a>';
                return $menu;
            })->make(true);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name',
            'email',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'agentsdatatable_' . time();
    }
}
