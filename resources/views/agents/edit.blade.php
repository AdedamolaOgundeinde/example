@extends('layouts.portal')
@php
$admin_agents_edit_manage = true;
$admin_agents_edit = true;
@endphp
@section('body')
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">
          
        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit/Add Agent</h3>
            </div>
              <div class="box-body">
                  {!! Form::open(['route'=> ['admin.agents.process_',  $id] ]) !!}
                  <div class="form-group">
                      {!! Form::label('name', 'Agent Full Name') !!}
                      {!! Form::text('name', $agent?$agent->user->name:null, ['class'=>'form-control', 'placeholder'=>'Ope Lautech']) !!}
                  </div>
                  <div class="form-group">
                      {!! Form::label('email', 'Agent Email Address') !!}
                      {!! Form::email('email', $agent?$agent->user->email:null, ['class'=>'form-control', 'placeholder'=>'abc@loftyinc.biz']) !!}
                  </div>
                  <div class="form-group">
                      {!! Form::label('password', 'Agent Access Password') !!}
                      {!! Form::password('password', ['class'=>'form-control']) !!}
                  </div>
                  <div class="form-group">
                      {!! Form::label('agent_poll_id', 'Agent Designated Poll Unit') !!}
                      {!! Form::select('agent_poll_id', [''=>'Select a poll unit', '2'=>'JamJam Boot'], $agent?$agent->agent_poll_id:null, ['class'=>'form-control']) !!}
                  </div>
                  <div class="box-footer">
                      @if($id)
                        <a href='{{ route('admin.agents.manage') }}' class='btn btn-warning'>Go Back</a>
                      @endif
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  {!! Form::close() !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection