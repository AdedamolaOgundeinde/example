@extends('layouts.portal')
@php
$admin_polls_edit_manage = true;
$admin_polls_edit = true;
@endphp
@section('body')
@section('body')
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">

        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit/Add Polling Units</h3>
            </div>
              <div class="box-body">
                  {!! Form::open(['route'=> ['admin.polls.process_',  $id] ]) !!}
                  <div class="form-group">
                    {!! Form::label('poll_state_id', 'State') !!}
                    {!! Form::select('poll_state_id', $states, request()->query('state'), ['class' => 'form-control', 'id'=>'state_select']); !!}
                  </div>
                  
                  <div class="form-group">
                    {!! Form::label('poll_lga_id', 'Lga') !!}
                    {!! Form::select('poll_lga_id', $lgas, request()->query('lga'), ['class' => 'form-control', 'id'=>'lga_select']); !!}
                  </div>
                  
                  <div class="form-group">
                    {!! Form::label('poll_ward_id', 'Ward') !!}
                    {!! Form::select('poll_ward_id', $wards, $poll?$poll->poll_ward_id:null, ['class' => 'form-control']); !!}
                  </div>
                  
                  <div class="form-group">
                    {!! Form::label('poll_name', 'Poll Name') !!}
                    {!! Form::text('poll_name', $poll?$poll->poll_name:null, ['class' => 'form-control']); !!}
                  </div>
                  <div class="form-group">
                      {!! Form::checkbox('poll_status', 'true', $poll?$poll->poll_status:false ) !!} Enable/Disable
                  </div>

                  <div class="box-footer">
                      @if($id)
                        <a href='{{ route('admin.polls.manage') }}' class='btn btn-warning'>Go Back</a>
                      @endif
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                  {!! Form::close() !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#state_select').change(function(){
            if(parseInt($(this).val()) > 0){
                document.location.href = location.pathname+'?state='+parseInt($(this).val());
            }
        });
        $('#lga_select').change(function(){
            if(parseInt($(this).val()) > 0){
                document.location.href = location.pathname+'?state='+parseInt($('#state_select').val())+
                        '&lga='+parseInt($(this).val());
            }
        });
    });
</script>
@endpush