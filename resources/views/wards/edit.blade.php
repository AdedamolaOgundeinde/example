@extends('layouts.portal')
@php
$admin_wards_edit_manage = true;
$admin_wards_edit = true;
@endphp
@section('body')
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">

        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit/Add Wards</h3>
            </div>
              <div class="box-body">
                  {!! Form::open(['route'=> ['admin.wards.process_',  $id] ]) !!}
                  
                  <div class="form-group">
                    {!! Form::label('lg_state_id', 'State') !!}
                    {!! Form::select('lg_state_id', $states, request()->query('state'), ['class' => 'form-control', 'id'=>'state_select']); !!}
                  </div>
                  
                  <div class="form-group">
                    {!! Form::label('ward_lga_id', 'Lga') !!}
                    {!! Form::select('ward_lga_id', $lgas, $ward?$ward->ward_lga_id:null, ['class' => 'form-control']); !!}
                  </div>
                  
                  <div class="form-group">
                      {!! Form::label('ward_name', 'Ward Name') !!}
                      {!! Form::text('ward_name', $ward?$ward->ward_name:null, ['class'=>'form-control', 'placeholder'=>'Ward B']) !!}
                  </div>
                  <div class="form-group">
                      {!! Form::checkbox('ward_status', 'true', $ward?$ward->ward_status:false ) !!} Enable/Disable
                  </div>

                  <div class="box-footer">
                      @if($id)
                        <a href='{{ route('admin.wards.manage') }}' class='btn btn-warning'>Go Back</a>
                      @endif
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                  {!! Form::close() !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#state_select').change(function(){
            if(parseInt($(this).val()) > 0){
                document.location.href = location.pathname+'?state='+parseInt($(this).val());
            }
        });
    });
</script>
@endpush
