@extends('layouts.portal')
@php
$admin_messaging_create = true;
@endphp
@section('body')
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">

        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Start a new Messaging to be delivered Now or Later</h3>
            </div>
              <div class="box-body">
                  {!! Form::open(['route'=> ['admin.messaging.create.process_'] ]) !!}


                  <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                  {!! Form::close() !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection
