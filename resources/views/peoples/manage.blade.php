@extends('layouts.portal')
@php
$admin_peoples_edit_manage = true;
$admin_peoples_manage = true;
@endphp
@section('body')
@push('css')
<link href="{{ asset('datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
@endpush
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">

        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage People in Database</h3>
            </div>
              <div class="box-body">
                  <div class='alert alert-info'>
                      <h5>Filter Data</h5>
                      <div class="row text-center">
                        {!! Form::open(['route'=> ['admin.peoples.manage'], 'method'=>'get', 'class'=>'form-inline' ]) !!}

                        <div class="form-group col-sm-4">
                          {!! Form::label('state', 'State') !!}
                          {!! Form::select('state', $states, request()->query('state'), ['class' => 'form-control', 'id'=>'state_select',  'style'=>'width:100%']); !!}
                        </div>

                        <div class="form-group col-sm-4">
                          {!! Form::label('lga', 'Lga') !!}
                          {!! Form::select('lga', $lgas, request()->query('lga'), ['class' => 'form-control', 'id'=>'lga_select', 'style'=>'width:100%']); !!}
                        </div>
                        
                        <div class="form-group col-sm-4">
                          {!! Form::label('gender', 'Gender') !!}
                          {!! Form::select('gender', [''=>'All', 'Male'=>'Male', 'Female'=>'Female'], request()->query('gender'), ['class' => 'form-control', 'id'=>'gender_select', 'style'=>'width:100%']); !!}
                        </div>
                        {!! Form::close() !!}
                      </div>
                  </div>
                  {!! $dataTable->table(['class' => 'table table-responsive'], false) !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection
@push('scripts')
    <script src="{{ asset('datatables/datatables.min.js', env('ssl', false)) }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
@push('scripts')
<script>
    $(document).ready(function(){
        $('#state_select').change(function(){
            document.location.href = location.pathname+'?state='+parseInt($(this).val());
        });
        $('#lga_select').change(function(){
            document.location.href = location.pathname+'?state='+parseInt($('#state_select').val())+
                    '&lga='+parseInt($(this).val());
        });
        $('#gender_select').change(function(){
            document.location.href = location.pathname+'?state='+parseInt($('#state_select').val())+
                    '&lga='+parseInt($('#lga_select').val())+'&gender='+$(this).val();
        });
    });
</script>
@endpush