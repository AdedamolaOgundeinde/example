@extends('layouts.portal')
@php
$admin_peoples_edit_manage = true;
$admin_peoples_edit = true;
@endphp
@section('body')
@section('body')
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">

        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit/Add Person</h3>
            </div>
              <div class="box-body">
                  {!! Form::open(['route'=> ['admin.peoples.process_',  $id] ]) !!}
                  
                  <div class="form-group">
                    {!! Form::label('poll_state_id', 'State') !!}
                    {!! Form::select('poll_state_id', $states, request()->query('state'), ['class' => 'form-control', 'id'=>'state_select']); !!}
                  </div>
                  
                  <div class="form-group">
                    {!! Form::label('people_lga_id', 'Lga') !!}
                    {!! Form::select('people_lga_id', $lgas, $people?$people->people_lga_id:null, ['class' => 'form-control', 'id'=>'lga_select']); !!}
                  </div>
                  
                  <div class="form-group">
                      {!! Form::label('people_phone', 'Phone Number') !!}
                      {!! Form::number('people_phone', $people?$people->people_phone:null, ['class'=>'form-control', 'placeholder'=>'08065748484', 'required']) !!}
                  </div>
                  <div class="form-group">
                    {!! Form::label('people_name', 'Person Name') !!}
                    {!! Form::text('people_name', $people?$people->people_name:null, ['class' => 'form-control', 'placeholder'=>'Moshood Abudl', 'required']); !!}
                  </div>
                  <div class="form-group">
                      {!! Form::label('people_age', 'Age') !!}
                      {!! Form::number('people_age', $people?$people->people_age:null, ['class'=>'form-control', 'placeholder'=>'18', 'required']) !!}
                  </div>
                  <div class="form-group">
                      {!! Form::label('people_gender', 'Gender') !!}
                      <label class="radio">
                      {!! Form::radio('people_gender', 'Male', $people?$people->people_gender=='Male'?'true':false:false); !!} Male
                      </label>
                      <label class="radio">
                      {!! Form::radio('people_gender', 'Female', $people?$people->people_gender=='Female'?'true':false:false); !!} Female
                      </label>
                  </div>
                  <div class="form-group">
                      {!! Form::label('people_email', 'E-mail Address (optional)') !!}
                      {!! Form::email('people_email', $people?$people->people_email:null, ['class'=>'form-control', 'placeholder'=>'abc@loftyinc.biz']) !!}
                  </div>

                  <div class="box-footer">
                      @if($id)
                        <a href='{{ route('admin.peoples.manage') }}' class='btn btn-warning'>Go Back</a>
                      @endif
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                  {!! Form::close() !!}
              </div>
          </div>
        </div>
        
        
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Bulk Upload</h3>
            </div>
              <div class="box-body">
                  {!! Form::open(['route'=> ['admin.peoples.process_',  $id] ]) !!}
                  
                  <p><a href='#'><i class='fa fa-download'></i>sample template</a></p>
                  <div class="form-group">
                      {!! Form::label('people_upload', 'Excel file upload') !!}
                      {!! Form::file('people_upload', ['class'=>'form-control']) !!}
                  </div>

                  <div class="box-footer">
                      <button type="submit" class="btn btn-info">Upload</button>
                  </div>

                  {!! Form::close() !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#state_select').change(function(){
            if(parseInt($(this).val()) > 0){
                document.location.href = location.pathname+'?state='+parseInt($(this).val());
            }
        });
    });
</script>
@endpush
