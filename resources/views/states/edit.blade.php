@extends('layouts.portal')
@php
$admin_states_edit_manage = true;
$admin_states_edit = true;
@endphp
@section('body')
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">

        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit/Add States</h3>
            </div>
              <div class="box-body">
                  {!! Form::open(['route'=> ['admin.states.process_',  $id] ]) !!}
                  <div class="form-group">
                      {!! Form::label('state_name', 'State Name') !!}
                      {!! Form::text('state_name', $state?$state->state_name:null, ['class'=>'form-control', 'placeholder'=>'Ondo']) !!}
                  </div>
                  <div class="form-group">
                      {!! Form::checkbox('state_status', 'true', $state?$state->state_status:false ) !!} Enable/Disable
                  </div>

                  <div class="box-footer">
                      @if($id)
                        <a href='{{ route('admin.states.manage') }}' class='btn btn-warning'>Go Back</a>
                      @endif
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                  {!! Form::close() !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection
