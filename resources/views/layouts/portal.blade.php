@extends('layouts.app')
@push('css')
<link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">
@endpush
@push('js')
    <!-- FastClick -->
    <script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
    @stack('scripts')
@endpush
@section('content')
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="{{ route('home') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b></b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b></b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">

                    <p>
                      {{ Auth::user()->name }}
                      <small>
                          @if(Auth::user()->is_admin)
                            Super Administrator
                          @else
                            Field Agent
                          @endif
                      </small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Sign out
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview {{ isset($admin_peoples_edit_manage)?'active menu-open':'' }}">
              <a href="#">
                <i class="fa fa-circle-o-notch"></i>
                <span>People</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ isset($admin_peoples_edit)?'active':'' }}"><a href="{{ route('admin.peoples.edit') }}"><i class="fa fa-circle-o"></i> Add</a></li>
                <li class="{{ isset($admin_peoples_manage)?'active':'' }}"><a href="{{ route('admin.peoples.manage') }}"><i class="fa fa-circle-o"></i> Manage</a></li>
              </ul>
            </li>
            <li class="treeview {{ isset($admin_states_edit_manage)?'active menu-open':'' }}">
              <a href="#">
                <i class="fa fa-circle-o-notch"></i>
                <span>States</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ isset($admin_states_edit)?'active':'' }}"><a href="{{ route('admin.states.edit') }}"><i class="fa fa-circle-o"></i> Add</a></li>
                <li class="{{ isset($admin_states_manage)?'active':'' }}"><a href="{{ route('admin.states.manage') }}"><i class="fa fa-circle-o"></i> Manage</a></li>
              </ul>
            </li>
            <li class="treeview {{ isset($admin_lgas_edit_manage)?'active menu-open':'' }}">
              <a href="#">
                <i class="fa fa-circle-o-notch"></i>
                <span>LGA</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ isset($admin_lgas_edit)?'active':'' }}"><a href="{{ route('admin.lgas.edit') }}"><i class="fa fa-circle-o"></i> Add</a></li>
                <li class="{{ isset($admin_lgas_manage)?'active':'' }}"><a href="{{ route('admin.lgas.manage') }}"><i class="fa fa-circle-o"></i> Manage</a></li>
              </ul>
            </li>
            <li class="treeview {{ isset($admin_wards_edit_manage)?'active menu-open':'' }}">
              <a href="#">
                <i class="fa fa-circle-o-notch"></i>
                <span>Wards</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ isset($admin_wards_edit)?'active':'' }}"><a href="{{ route('admin.wards.edit') }}"><i class="fa fa-circle-o"></i> Add</a></li>
                <li class="{{ isset($admin_wards_manage)?'active':'' }}"><a href="{{ route('admin.wards.manage') }}"><i class="fa fa-circle-o"></i> Manage</a></li>
              </ul>
            </li>
            <li class="treeview  {{ isset($admin_polls_edit_manage)?'active menu-open':'' }}">
              <a href="#">
                <i class="fa fa-circle-o-notch"></i>
                <span>Polling Units</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ isset($admin_polls_edit)?'active':'' }}"><a href="{{ route('admin.polls.edit') }}"><i class="fa fa-circle-o"></i> Add</a></li>
                <li class="{{ isset($admin_polls_manage)?'active':'' }}"><a href="{{ route('admin.polls.manage') }}"><i class="fa fa-circle-o"></i> Manage</a></li>
              </ul>
            </li>
            <li class="treeview {{ isset($admin_agents_edit_manage)?'active menu-open':'' }}">
              <a href="#">
                <i class="fa fa-circle-o-notch"></i>
                <span>Agents</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="{{ isset($admin_agents_edit)?'active':'' }}"><a href="{{ route('admin.agents.edit') }}"><i class="fa fa-circle-o"></i> Add</a></li>
                <li class="{{ isset($admin_agents_manage)?'active':'' }}"><a href="{{ route('admin.agents.manage') }}"><i class="fa fa-circle-o"></i> Manage</a></li>
              </ul>
            </li>
            <li class="header">Messaging &amp; Reports</li>
            <li class="{{ isset($admin_messaging_create)?'active':'' }}"><a href="{{ route('admin.messaging.create') }}"><i class="fa fa-inbox text-blue"></i> <span>New or Schedule</span></a></li>
            <li><a href="#"><i class="fa fa-mercury text-green"></i> <span>Report &amp; Feedbacks</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            @yield('body')

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2017 .</strong> All rights reserved.
      </footer>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
@endsection
