@extends('layouts.portal')
@php
$admin_lgas_edit_manage = true;
$admin_lgas_edit = true;
@endphp
@section('body')
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">

        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit/Add LGAs</h3>
            </div>
              <div class="box-body">
                  {!! Form::open(['route'=> ['admin.lgas.process_',  $id] ]) !!}

                  <div class="form-group">
                    {!! Form::label('lga_state_id', 'State') !!}
                    {!! Form::select('lga_state_id', $states, $lga?$lga->lga_state_id:null, ['class' => 'form-control']); !!}
                  </div>
                  <div class="form-group">
                      {!! Form::label('lga_name', 'LGA Name') !!}
                      {!! Form::text('lga_name', $lga?$lga->lga_name:null, ['class'=>'form-control', 'placeholder'=>'Akindoyin']) !!}
                  </div>
                  <div class="form-group">
                      {!! Form::checkbox('lga_status', 'true', $lga?$lga->lga_status:false ) !!} Enable/Disable
                  </div>

                  <div class="box-footer">
                      @if($id)
                        <a href='{{ route('admin.lgas.manage') }}' class='btn btn-warning'>Go Back</a>
                      @endif
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                  {!! Form::close() !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection
