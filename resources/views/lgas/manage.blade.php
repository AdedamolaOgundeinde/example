@extends('layouts.portal')
@php
$admin_lgas_edit_manage = true;
$admin_lgas_manage = true;
@endphp
@section('body')
@push('css')
<link href="{{ asset('datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
@endpush
<section class="content">
    <div class="panel-heading"></div>
      <div class="row">

        @if($errors->first())
        <div class='text-center alert alert-{{ $errors->first('status') }}'>{{ $errors->first('message') }}</div>
        @endif
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Manage Lgas</h3>
            </div>
              <div class="box-body">
                  {!! $dataTable->table(['class' => 'table table-responsive'], false) !!}
              </div>
          </div>
        </div>
      </div>
</section>
@endsection
@push('scripts')
    <script src="{{ asset('datatables/datatables.min.js', env('ssl', false)) }}"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    {!! $dataTable->scripts() !!}
@endpush
